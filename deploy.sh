#!/usr/bin/env bash
touch /home/rdw/diceroller_users.log || true
docker login -u $REGISTRY_USER -p $REGISTRY_PASSWORD $REGISTRY_URL
docker pull registry.gitlab.com/reddragonway/vtm5-dice-roller/vtm5dicebot:latest
docker stop vtm5dicebot || true
docker rm vtm5dicebot || true
docker run -d --name vtm5dicebot \
    -v /home/rdw/diceroller_users.log:/app/diceroller_users.log \
    --restart always \
    --pull always \
    registry.gitlab.com/reddragonway/vtm5-dice-roller/vtm5dicebot:latest
docker image prune -f
