# Общие команды

# импорт библиотек
from aiogram import Router, types
from aiogram.filters import Command


# экземпляр роутера
router = Router()


# обработка команды старт
@router.message(Command(commands=["start", "старт"]))
async def cmd_start(message: types.Message):
    kb = [
        [types.KeyboardButton(text="Дайсроллер")],
        [types.KeyboardButton(text="Помощь")]
    ]
    keyboard = types.ReplyKeyboardMarkup(keyboard=kb, resize_keyboard=True)
    await message.answer("Приветствую! Я бот - дайсроллер.\n"
                         "Эмулирую броски d10-костей настольно-ролевой игры Vampire: The Masquerade 5th Edition.\n"
                         "Выберите пункт меню", reply_markup=keyboard)
