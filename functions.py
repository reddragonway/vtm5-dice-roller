# Функции логики бота

# импорт библиотек
from random import randint


# функция генерации случайного значения дайса
def dice_value():
    return randint(1, 10)


# функция генерации пула
def roll(pull_value):
    return tuple(dice_value() for _ in range(pull_value))


# функция подсчета значений броска
def count_hit(dice_, blood_dice_):
    count = 0
    brutal_failure_ = False
    breakthrough = 0
    triumph_ = False
    blood_count = 0
    blood_failure_ = False
    blood_mark = False
    blood_triumph_ = False
    for _ in dice_:
        if _ in range(6, 10):
            count = count + 1
        elif _ == 10:
            breakthrough = breakthrough + 1
            if breakthrough in range(2, 21, 2):
                triumph_ = True
                count = count + 3
            else:
                count = count + 1
    for _ in blood_dice_:
        if _ == 1:
            blood_failure_ = True
        elif _ in range(6, 10):
            blood_count = blood_count + 1
        elif _ == 10:
            blood_mark = True
            breakthrough = breakthrough + 1
            if breakthrough in range(2, 21, 2):
                triumph_ = True
                blood_triumph_ = True
                count = count + 3
            else:
                count = count + 1
    final_count_ = count + blood_count
    if final_count_ == 0:
        brutal_failure_ = True
    if triumph_ is True and blood_mark is True:
        blood_triumph_ = True
    return final_count_, brutal_failure_, triumph_, blood_failure_, blood_triumph_


# функция броска воли (переводит пул из кортежа в список)
def willpower_reroll(dice_, number):
    dice_ = list(dice_)
    dice_ = willpower_dice(dice_, number)
    return dice_


# вспомогательная функция для броска воли (блокирует выбранные значения)
def willpower_dice(dice_, number_):
    for i, n in enumerate(dice_):
        if n == number_:
            dice_[i] = str(randint(1, 10)) + "*"
            break
    return dice_


# функция для перевода пула броска воли обратно в кортеж
def dice_to_tuple(dice_):
    dice_ = str(dice_)[1:-1].replace('*', "")
    dice_ = dice_.replace("'", "")
    dice_ = ''.join(dice_.split(','))
    dice_ = tuple(int(item) for item in dice_.split(' '))
    return dice_
