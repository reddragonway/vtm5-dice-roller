FROM python:3.10-slim
ARG TOKEN=${TOKEN}
ENV TOKEN=${TOKEN}
WORKDIR /app
COPY bot.py bot.py
COPY functions.py functions.py
COPY ./handlers ./handlers
COPY requirements.txt requirements.txt
RUN apt-get update &&\
    apt-get install -y nano procps &&\
    pip3 install -r requirements.txt
CMD [ "python3", "./bot.py"]
