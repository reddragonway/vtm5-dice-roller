#!/usr/bin/env python3

# импорт библиотек
import os
import asyncio
import logging
from aiogram import Bot, Dispatcher
from handlers import common, diceroller


# токен
TOKEN = os.environ["TOKEN"]


# главная функция
async def main():
    logging.basicConfig(level=logging.WARNING, format="%(asctime)s - %(levelname)s - %(name)s - %(message)s")
    bot = Bot(token=TOKEN)
    dp = Dispatcher()
    dp.include_router(common.router)
    dp.include_router(diceroller.router)
    await dp.start_polling(bot, allowed_updates=dp.resolve_used_update_types())


# запуск бота
if __name__ == '__main__':
    asyncio.run(main())
