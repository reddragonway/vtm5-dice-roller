# Эмулятор дайсроллера

# импорт библиотек
from aiogram import Router, types, F
from aiogram.utils.keyboard import ReplyKeyboardBuilder
from aiogram.fsm.context import FSMContext
from aiogram.fsm.state import State, StatesGroup
from functions import roll, count_hit, willpower_reroll, dice_to_tuple


# экземпляр роутера
router = Router()


# создание состояний для FSM
class DiceRoller(StatesGroup):
    choosing_dicepool = State()
    choosing_hunger = State()
    choosing_difficulty = State()
    choosing_willpower = State()
    choosing_willpower_quantity = State()
    choosing_willpower_quantity_1 = State()
    choosing_willpower_quantity_2 = State()
    choosing_willpower_quantity_3 = State()
    willpower_roll = State()


# глобальный словарь пользователей
users = {}


# функция проверки пользователей, запись лога,
# установка переменных пользователя на дефолтные значения: dice = None, blood_dice = None, difficulty = None, willpower_off_flag = False
def check_users(message_chat_id, message_chat_username):
    global users
    if message_chat_id not in users:
        with open('diceroller_users.log', 'a') as f:
            f.write(str(message_chat_username) + ":" + str(message_chat_id) + "\n")
        value = [None, None, None, False]
        users.update({message_chat_id: value})


# доступные значения переменных
available_dicepool_and_difficulty = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10",
                                     "11", "12", "13", "14", "15", "16", "17", "18", "19", "20"]
available_hunger = ["0", "1", "2", "3", "4", "5"]
available_willpower_quantity = ["1", "2", "3"]
available_willpower_values = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10"]


# обработка старта дайсроллера (предлагаем выбрать дайспул)
@router.message(lambda message: message.text == "Дайсроллер" or message.text == "Новый бросок")
async def start_diceroller(message: types.Message, state: FSMContext):
    await state.clear()
    global users
    message_chat_id = message.chat.id
    message_from_user_username = message.from_user.username
    check_users(message_chat_id, message_from_user_username)
    builder = ReplyKeyboardBuilder()
    for i in range(1, 21):
        builder.add(types.KeyboardButton(text=str(i)))
    builder.adjust(5)
    await message.answer("Выберите дайспул", reply_markup=builder.as_markup(resize_keyboard=True))
    await state.set_state(DiceRoller.choosing_dicepool)


# получаем дайспул -> предлагаем выбрать уровень голода
@router.message(DiceRoller.choosing_dicepool, F.text.in_(available_dicepool_and_difficulty))
async def choose_dicepool(message: types.Message, state: FSMContext):
    await state.update_data(chosen_dicepool=message.text)
    user_data = await state.get_data()
    builder = ReplyKeyboardBuilder()
    for i in range(0, 6):
        builder.add(types.KeyboardButton(text=str(i)))
    builder.adjust(3)
    await message.answer(f"[ дайспул: {user_data['chosen_dicepool']} ]")
    await message.answer("Выберите уровень голода", reply_markup=builder.as_markup(resize_keyboard=True))
    await state.set_state(DiceRoller.choosing_hunger)


# получаем уровень голода -> предлагаем выбрать уровень сложности
@router.message(DiceRoller.choosing_hunger, F.text.in_(available_hunger))
async def choose_hunger(message: types.Message, state: FSMContext):
    await state.update_data(chosen_hunger=message.text)
    user_data = await state.get_data()
    dice_pull = int(user_data['chosen_dicepool'])
    diff_max = dice_pull + 1
    builder = ReplyKeyboardBuilder()
    for i in range(1, diff_max):
        builder.add(types.KeyboardButton(text=str(i)))
    builder.adjust(5)
    await message.answer(f"[ дайспул: {user_data['chosen_dicepool']} | голод: {user_data['chosen_hunger']} ]")
    await message.answer("Выберите уровень сложности", reply_markup=builder.as_markup(resize_keyboard=True))
    await state.set_state(DiceRoller.choosing_difficulty)


# получаем уровень сложности -> выводим результаты броска дайсов
@router.message(DiceRoller.choosing_difficulty, F.text.in_(available_dicepool_and_difficulty))
async def choose_difficulty(message: types.Message, state: FSMContext):
    await state.update_data(chosen_difficulty=message.text)
    user_data = await state.get_data()
    kb = [
        [types.KeyboardButton(text="Бросок воли")],
        [types.KeyboardButton(text="Новый бросок")],
        [types.KeyboardButton(text="Помощь")]
    ]
    keyboard = types.ReplyKeyboardMarkup(keyboard=kb, resize_keyboard=True)
    await message.answer(f"[ дайспул: {user_data['chosen_dicepool']} | голод: {user_data['chosen_hunger']} |"
                         f" сложность: {user_data['chosen_difficulty']} ]")
    await message.answer(f"|| >> ...БРОСАЕМ ДАЙСЫ... << ||")
    dice_pull = int(user_data['chosen_dicepool'])
    hunger = int(user_data['chosen_hunger'])
    global users
    message_chat_id = message.chat.id
    if hunger >= dice_pull:
        hunger = dice_pull
        dice_pull = dice_pull - hunger
        users[message_chat_id][3] = True
    else:
        dice_pull = dice_pull - hunger
    users[message_chat_id][2] = int(user_data['chosen_difficulty'])
    users[message_chat_id][0] = roll(dice_pull)
    users[message_chat_id][1] = roll(hunger)
    final_count, brutal_failure, triumph, blood_failure, blood_triumph = count_hit(users[message_chat_id][0], users[message_chat_id][1])
    await message.answer(f"Количество успехов: {final_count}")
    if final_count >= users[message_chat_id][2] and triumph is True and blood_triumph is True:
        await message.answer(f"Дайсы: {''.join(str(users[message_chat_id][0])[1:-1].split(','))}\n"
                             f"Голод: {''.join(str(users[message_chat_id][1])[1:-1].split(','))}\n")
        await message.answer(f"Успех! Кровавый триумф!", reply_markup=keyboard)
    elif final_count >= users[message_chat_id][2] and triumph is True:
        await message.answer(f"Дайсы: {''.join(str(users[message_chat_id][0])[1:-1].split(','))}\n"
                             f"Голод: {''.join(str(users[message_chat_id][1])[1:-1].split(','))}\n")
        await message.answer(f"Успех! Триумф!", reply_markup=keyboard)
    elif final_count >= users[message_chat_id][2]:
        await message.answer(f"Дайсы: {''.join(str(users[message_chat_id][0])[1:-1].split(','))}\n"
                             f"Голод: {''.join(str(users[message_chat_id][1])[1:-1].split(','))}\n")
        await message.answer(f"Успех!", reply_markup=keyboard)
    elif final_count < users[message_chat_id][2] and brutal_failure is True and blood_failure is True:
        await message.answer(f"Дайсы: {''.join(str(users[message_chat_id][0])[1:-1].split(','))}\n"
                             f"Голод: {''.join(str(users[message_chat_id][1])[1:-1].split(','))}\n")
        await message.answer(f"Неудача! Кровавый провал!", reply_markup=keyboard)
    elif final_count < users[message_chat_id][2] and brutal_failure is True:
        await message.answer(f"Дайсы: {''.join(str(users[message_chat_id][0])[1:-1].split(','))}\n"
                             f"Голод: {''.join(str(users[message_chat_id][1])[1:-1].split(','))}\n")
        await message.answer(f"Неудача! Жестокий провал!", reply_markup=keyboard)
    else:
        await message.answer(f"Дайсы: {''.join(str(users[message_chat_id][0])[1:-1].split(','))}\n"
                             f"Голод: {''.join(str(users[message_chat_id][1])[1:-1].split(','))}\n")
        await message.answer(f"Неудача!", reply_markup=keyboard)
    await state.set_state(DiceRoller.choosing_willpower)


# обработка старта броска воли (предлагаем выбрать количество дайсов для броска)
@router.message(DiceRoller.choosing_willpower, F.text == "Бросок воли")
async def choose_willpower(message: types.Message, state: FSMContext):
    global users
    message_chat_id = message.chat.id
    if users[message_chat_id][3]:
        kb = [
            [types.KeyboardButton(text="Новый бросок")],
            [types.KeyboardButton(text="Помощь")]
        ]
        keyboard = types.ReplyKeyboardMarkup(keyboard=kb, resize_keyboard=True)
        await message.answer("Бросок воли не может быть выполнен для костей пула голода",
                             reply_markup=keyboard)
        await state.clear()
        users[message_chat_id][3] = False
    else:
        user_data = await state.get_data()
        dice_pull = int(user_data['chosen_dicepool'])
        hunger = int(user_data['chosen_hunger'])
        dice_pull = dice_pull - hunger
        builder = ReplyKeyboardBuilder()
        if dice_pull == 1 and dice_pull:
            for i in range(1, 2):
                builder.add(types.KeyboardButton(text=str(i)))
            builder.adjust(1)
        elif dice_pull == 2:
            for i in range(1, 3):
                builder.add(types.KeyboardButton(text=str(i)))
            builder.adjust(2)
        elif dice_pull >=3:
            for i in range(1, 4):
                builder.add(types.KeyboardButton(text=str(i)))
            builder.adjust(3)
        await message.answer("Выберите количество дайсов для броска воли",
                             reply_markup=builder.as_markup(resize_keyboard=True))
        await state.set_state(DiceRoller.choosing_willpower_quantity)


# получаем количество дайсов для броска -> предлагаем выбрать значение первого дайса
@router.message(DiceRoller.choosing_willpower_quantity, F.text.in_(available_willpower_quantity))
async def choose_willpower_quantity(message: types.Message, state: FSMContext):
    await state.update_data(chosen_willpower=message.text)
    user_data = await state.get_data()
    builder = ReplyKeyboardBuilder()
    for i in range(1, 11):
        builder.add(types.KeyboardButton(text=str(i)))
    builder.adjust(5)
    if int(user_data['chosen_willpower']) == 1:
        await message.answer("Выберите значение дайса:", reply_markup=builder.as_markup(resize_keyboard=True))
    elif int(user_data['chosen_willpower']) in (2, 3):
        await message.answer("Выберите значение первого дайса:", reply_markup=builder.as_markup(resize_keyboard=True))
    await state.set_state(DiceRoller.choosing_willpower_quantity_1)


# получаем значение первого дайса -> предлагаем выбрать значение второго дайса
@router.message(DiceRoller.choosing_willpower_quantity_1, F.text.in_(available_willpower_values))
async def choose_willpower_quantity_1(message: types.Message, state: FSMContext):
    await state.update_data(chosen_willpower_1=message.text)
    user_data = await state.get_data()
    builder = ReplyKeyboardBuilder()
    for i in range(1, 11):
        builder.add(types.KeyboardButton(text=str(i)))
    builder.adjust(5)
    global users
    message_chat_id = message.chat.id
    if int(user_data['chosen_willpower_1']) not in users[message_chat_id][0]:
        await message.answer("Указанного дайса нету в текущем пуле. Введите значение дайса: ",
                             reply_markup=builder.as_markup(resize_keyboard=True))
    else:
        number = int(user_data['chosen_willpower_1'])
        users[message_chat_id][0] = willpower_reroll(users[message_chat_id][0], number)
        if int(user_data['chosen_willpower']) in (2, 3):
            await message.answer("Выберите значение второго дайса:",
                                 reply_markup=builder.as_markup(resize_keyboard=True))
            await state.set_state(DiceRoller.choosing_willpower_quantity_2)
        else:
            await state.set_state(DiceRoller.willpower_roll)
            await choose_willpower_roll(message, state)


# получаем значение второго дайса -> предлагаем выбрать значение третьего дайса
@router.message(DiceRoller.choosing_willpower_quantity_2, F.text.in_(available_willpower_values))
async def choose_willpower_quantity_2(message: types.Message, state: FSMContext):
    await state.update_data(chosen_willpower_2=message.text)
    user_data = await state.get_data()
    builder = ReplyKeyboardBuilder()
    for i in range(1, 11):
        builder.add(types.KeyboardButton(text=str(i)))
    builder.adjust(5)
    global users
    message_chat_id = message.chat.id
    if int(user_data['chosen_willpower_2']) not in users[message_chat_id][0]:
        await message.answer("Указанного дайса нету в текущем пуле. Введите значение дайса: ",
                             reply_markup=builder.as_markup(resize_keyboard=True))
    else:
        number = int(user_data['chosen_willpower_2'])
        users[message_chat_id][0] = willpower_reroll(users[message_chat_id][0], number)
        if int(user_data['chosen_willpower']) == 3:
            await message.answer("Выберите значение третьего дайса:",
                                 reply_markup=builder.as_markup(resize_keyboard=True))
            await state.set_state(DiceRoller.choosing_willpower_quantity_3)
        else:
            await state.set_state(DiceRoller.willpower_roll)
            await choose_willpower_roll(message, state)


# получаем значение третьего дайса
@router.message(DiceRoller.choosing_willpower_quantity_3, F.text.in_(available_willpower_values))
async def choose_willpower_quantity_3(message: types.Message, state: FSMContext):
    await state.update_data(chosen_willpower_3=message.text)
    user_data = await state.get_data()
    builder = ReplyKeyboardBuilder()
    for i in range(1, 11):
        builder.add(types.KeyboardButton(text=str(i)))
    builder.adjust(5)
    global users
    message_chat_id = message.chat.id
    if int(user_data['chosen_willpower_3']) not in users[message_chat_id][0]:
        await message.answer("Указанного дайса нету в текущем пуле. Введите значение дайса: ",
                             reply_markup=builder.as_markup(resize_keyboard=True))
    else:
        number = int(user_data['chosen_willpower_3'])
        users[message_chat_id][0] = willpower_reroll(users[message_chat_id][0], number)
        await state.set_state(DiceRoller.willpower_roll)
        await choose_willpower_roll(message, state)


# выводим результаты броска воли
@router.message(DiceRoller.willpower_roll)
async def choose_willpower_roll(message: types.Message, state: FSMContext):
    user_data = await state.get_data()
    kb = [
        [types.KeyboardButton(text="Новый бросок")],
        [types.KeyboardButton(text="Помощь")]
    ]
    keyboard = types.ReplyKeyboardMarkup(keyboard=kb, resize_keyboard=True)
    await message.answer(f"[ дайспул: {user_data['chosen_dicepool']} | голод: {user_data['chosen_hunger']} |"
                         f" сложность: {user_data['chosen_difficulty']} ]")
    await message.answer(f"|| >> ...БРОСОК ВОЛИ... << ||")
    global users
    message_chat_id = message.chat.id
    difficulty = users[message_chat_id][2]
    blood_dice = users[message_chat_id][1]
    dice = dice_to_tuple(users[message_chat_id][0])
    final_count, brutal_failure, triumph, blood_failure, blood_triumph = count_hit(dice, blood_dice)
    await message.answer(f"Количество успехов: {final_count}")
    if final_count >= difficulty and triumph is True and blood_triumph is True:
        await message.answer(f"Дайсы: {''.join(str(dice)[1:-1].split(','))}\n"
                             f"Голод: {''.join(str(blood_dice)[1:-1].split(','))}\n")
        await message.answer(f"Успех! Кровавый триумф!", reply_markup=keyboard)
    elif final_count >= difficulty and triumph is True:
        await message.answer(f"Дайсы: {''.join(str(dice)[1:-1].split(','))}\n"
                             f"Голод: {''.join(str(blood_dice)[1:-1].split(','))}\n")
        await message.answer(f"Успех! Триумф!", reply_markup=keyboard)
    elif final_count >= difficulty:
        await message.answer(f"Дайсы: {''.join(str(dice)[1:-1].split(','))}\n"
                             f"Голод: {''.join(str(blood_dice)[1:-1].split(','))}\n")
        await message.answer(f"Успех!", reply_markup=keyboard)
    elif final_count < difficulty and brutal_failure is True and blood_failure is True:
        await message.answer(f"Дайсы: {''.join(str(dice)[1:-1].split(','))}\n"
                             f"Голод: {''.join(str(blood_dice)[1:-1].split(','))}\n")
        await message.answer(f"Неудача! Кровавый провал!", reply_markup=keyboard)
    elif final_count < difficulty and brutal_failure is True:
        await message.answer(f"Дайсы: {''.join(str(dice)[1:-1].split(','))}\n"
                             f"Голод: {''.join(str(blood_dice)[1:-1].split(','))}\n")
        await message.answer(f"Неудача! Жестокий провал!", reply_markup=keyboard)
    else:
        await message.answer(f"Дайсы: {''.join(str(dice)[1:-1].split(','))}\n"
                             f"Голод: {''.join(str(blood_dice)[1:-1].split(','))}\n")
        await message.answer(f"Неудача!", reply_markup=keyboard)
    await state.clear()


# вывод справочной информации
@router.message(lambda message: message.text == "Помощь")
async def cmd_help(message: types.Message):
    await message.answer('Бот - дайсроллер, эмулирует броски d10-костей настольно-ролевой игры '
                         'Vampire: The Masquerade 5th Edition.\n'
                         'Чтобы перезагрузить бота введите /start или /старт')
    await message.answer('|| >> ...ОСНОВНЫЕ ТЕРМИНЫ... << ||')
    await message.answer('Дайспул - общее количество бросаемых костей.\n'
                         'Уровень голода - количество "кровавых" костей, которые заменяют обычные кости из общего '
                         'дайспула. Могут отсутствовать при броске.\n'
                         'Уровень сложности - количество "успехов", необходимых для прохождения проверки '
                         'или испытания.')
    await message.answer('Бросок воли - возможность перебросить от 1 до 3 костей.')
    await message.answer('Успех - кость со значением от 6 и выше.\n'
                         'Триумф - пара костей, значение каждой из которых равно 10. Каждая пара "десяток" '
                         'равна 4 успехам.\n'
                         'Жесткий провал - если при броске не выпало ни одного успеха.')
    await message.answer('Кровавый триумф - пара костей из пула голода, значение каждой из которых равно 10, либо одна '
                         '"десятка" из пула голода в паре с "десяткой" из обычного дайспула.\n'
                         'Кровавый провал - жесткий провал, в пуле которого присутствует кость из пула голода '
                         'со значением равным 1.')


# обработка неизвестных команд
@router.message()
async def echo(message: types.Message):
    await message.answer("Неизвестная команда. Пожалуйста, воспользуйтесь меню")
